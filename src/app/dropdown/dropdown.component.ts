import { FormControl } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
})
export class DropdownComponent implements OnInit {
  @Input() region: FormControl;
  regions: string[] = ['Africa', 'Americas', 'Asia', 'Europe', 'Oceania'];
  constructor() {}

  ngOnInit(): void {}
}
