import { Country } from './../country';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-country-grid',
  templateUrl: './country-grid.component.html',
  styleUrls: ['./country-grid.component.scss'],
})
export class CountryGridComponent implements OnInit {
  @Input() countries: Country[];
  constructor() {}

  ngOnInit(): void {}
}
