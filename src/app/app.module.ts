import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SearchFormComponent } from './search-form/search-form.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { CountryGridComponent } from './country-grid/country-grid.component';
import { CountryGridItemComponent } from './country-grid-item/country-grid-item.component';
import { CountriesPageComponent } from './countries-page/countries-page.component';
import { LayoutComponent } from './layout/layout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CountryDetailComponent } from './country-detail/country-detail.component';
import { CountryDetailPageComponent } from './country-detail-page/country-detail-page.component';
import { JoinPipe } from './join.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SearchFormComponent,
    DropdownComponent,
    SearchBarComponent,
    CountryGridComponent,
    CountryGridItemComponent,
    CountriesPageComponent,
    LayoutComponent,
    CountryDetailComponent,
    CountryDetailPageComponent,
    JoinPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [
    HeaderComponent,
    SearchFormComponent,
    DropdownComponent,
    SearchBarComponent,
    CountryGridComponent,
    CountryGridItemComponent,
    CountriesPageComponent,
    LayoutComponent,
    CountryDetailComponent,
  ],
})
export class AppModule {}
