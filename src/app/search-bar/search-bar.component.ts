import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent implements OnInit {
  @Output() region = new EventEmitter<string>();
  @Output() query = new EventEmitter<string>();

  builder = new FormBuilder();
  form;
  constructor() {
    this.form = this.builder.group({
      region: [],
      query: [],
    });

    this.form.controls.region.valueChanges.subscribe((value) => {
      this.region.emit(value);
    });
    this.form.controls.query.valueChanges.subscribe((value) => {
      this.query.emit(value);
    });
  }

  ngOnInit(): void {}
}
