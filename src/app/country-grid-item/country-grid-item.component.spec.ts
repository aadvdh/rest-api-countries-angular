import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryGridItemComponent } from './country-grid-item.component';

describe('CountryGridItemComponent', () => {
  let component: CountryGridItemComponent;
  let fixture: ComponentFixture<CountryGridItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryGridItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryGridItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
