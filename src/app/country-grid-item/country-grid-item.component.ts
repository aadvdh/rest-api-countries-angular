import { Country } from './../country';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-country-grid-item',
  templateUrl: './country-grid-item.component.html',
  styleUrls: ['./country-grid-item.component.scss'],
})
export class CountryGridItemComponent implements OnInit {
  @Input() country: Country;
  constructor() {}

  ngOnInit(): void {}
}
