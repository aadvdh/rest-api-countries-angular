import { Country } from './country';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root',
})
export class CountriesApiService {
  constructor(private http: HttpClient) {}

  getAllCountries(): Observable<Country[]> {
    return this.http.get<Country[]>('https://restcountries.eu/rest/v2/all');
  }

  getCountryByAlphaCode(alphaCode: string): Observable<Country> {
    return this.http.get<Country>(
      `http://restcountries.eu/rest/v2/alpha/${alphaCode}`
    );
  }
}
