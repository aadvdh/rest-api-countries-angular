import { Observable } from 'rxjs/internal/Observable';
import { Country } from './../country';
import { CountriesApiService } from './../countries-api.service';
import {
  Component,
  OnInit,
  ɵCompiler_compileModuleSync__POST_R3__,
} from '@angular/core';

@Component({
  selector: 'app-countries-page',
  templateUrl: './countries-page.component.html',
  styleUrls: ['./countries-page.component.scss'],
})
export class CountriesPageComponent implements OnInit {
  countries: Country[] = [];
  selectedRegion: string = null;
  currentQuery: string = '';
  filteredCountries: Country[] = [];
  constructor(private api: CountriesApiService) {}

  ngOnInit(): void {
    this.api.getAllCountries().subscribe(
      (value) => (this.countries = value),
      () => {},
      () => {
        this.filteredCountries = this.countries;
      }
    );
  }

  onRegionChange(region: string) {
    this.selectedRegion = region;

    this.filteredCountries = this.getFilteredCountries(
      this.currentQuery,
      region
    );
  }
  onQueryChange(query: string) {
    this.currentQuery = query;
    this.filteredCountries = this.getFilteredCountries(
      query,
      this.selectedRegion
    );
  }
  getFilteredCountries(query: string, region: string) {
    let filtered = this.countries;
    if (query == '' && region == null) {
      return this.countries;
    }
    if (query != '') {
      filtered = this.getCountriesFilteredByName(query, filtered);
    }
    if (region !== null) {
      filtered = this.getCountriesFilteredByRegion(region, filtered);
    }

    return filtered;
  }
  getCountriesFilteredByRegion(region: string, countries: Country[]) {
    return countries.filter((country) => {
      return country.region == region;
    });
  }

  getCountriesFilteredByName(name: string, countries: Country[]) {
    return countries.filter((country) => {
      return country.name.toLowerCase().startsWith(name.toLowerCase());
    });
  }
}
