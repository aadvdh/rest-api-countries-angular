import { CountriesPageComponent } from './countries-page/countries-page.component';
import { CountryDetailPageComponent } from './country-detail-page/country-detail-page.component';
import { CountryDetailComponent } from './country-detail/country-detail.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: CountriesPageComponent },
  { path: 'countries/:code', component: CountryDetailPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
