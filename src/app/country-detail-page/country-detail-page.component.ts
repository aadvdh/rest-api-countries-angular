import { CountriesApiService } from './../countries-api.service';
import { Country } from './../country';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-country-detail-page',
  templateUrl: './country-detail-page.component.html',
  styleUrls: ['./country-detail-page.component.scss'],
})
export class CountryDetailPageComponent implements OnInit {
  country: Country;
  alphaCode: string;
  constructor(
    private countriesApi: CountriesApiService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.alphaCode = params.code;
      this.getCountryByAlphaCode();
    });
  }

  getCountryByAlphaCode() {
    this.countriesApi
      .getCountryByAlphaCode(this.alphaCode)
      .subscribe((country) => (this.country = country));
  }
}
