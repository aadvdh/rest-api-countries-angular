import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'join',
})
export class JoinPipe implements PipeTransform {
  transform(value: any, property): unknown {
    let arr = value.map((val) => val[property]);

    return arr.join(', ');
  }
}
